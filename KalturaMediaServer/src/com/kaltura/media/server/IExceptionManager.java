package com.kaltura.media.server;

public interface IExceptionManager extends IManager {

	public void handleException(Exception e);

}
