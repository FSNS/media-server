package com.kaltura.media.server;

public interface IManager {

	public void init() throws KalturaManagerException;

	public void stop();
}
